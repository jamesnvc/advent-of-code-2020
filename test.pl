:- module(test, [run/2]).

:- use_module(library(simplex)).

run(Amount, [Pennies, Nickels, Dimes, Quarters]) :-
    gen_state(S0),
    setup_constraints(Amount, S0, S1),
    variable_value(S1, c(1), Pennies),
    variable_value(S1, c(5), Nickels),
    variable_value(S1, c(10), Dimes),
    variable_value(S1, c(25), Quarters).

% what's the fewest coins to pay a given amount?
setup_constraints(Amount) -->
    constraint([c(1), 5 * c(5), 10 * c(10), 25 * c(25)] = Amount),
    constraint(integral(c(1))),
    constraint(integral(c(5))),
    constraint(integral(c(10))),
    constraint(integral(c(25))),
    minimize([c(1), c(5), c(10), c(25)]).
