:- module(utils, [read_input/2]).

:- use_module(library(readutil), [read_line_to_string/2]).

read_input(File, Lines) :-
    setup_call_cleanup(
        open(File, read, S),
        read_line_numbers(S, Lines),
        close(S)
    ).

read_line_numbers(S, L0) :-
    read_line_to_string(S, LineS),
    ( LineS == end_of_file
    -> L0 = []
    ;  ( number_string(Line, LineS),
         L0 = [Line|L1],
         read_line_numbers(S, L1)
       )
    ).
