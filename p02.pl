:- module(p02, [solve1/2, solve2/2]).

/*
Your flight departs in a few days from the coastal airport; the easiest way down to the coast from here is via toboggan.

The shopkeeper at the North Pole Toboggan Rental Shop is having a bad day. "Something's wrong with our computers; we can't log in!" You ask if you can take a look.

Their password database seems to be a little corrupted: some of the passwords wouldn't have been allowed by the Official Toboggan Corporate Policy that was in effect when they were chosen.

To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.

For example, suppose you have the following list:

1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc

Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.

In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of their respective policies.

How many passwords are valid according to their policies?

--- Part Two ---

While it appears you validated the passwords correctly, they don't seem to be what the Official Toboggan Corporate Authentication System is expecting.

The shopkeeper suddenly realizes that he just accidentally explained the password policy rules from his old job at the sled rental place down the street! The Official Toboggan Corporate Policy actually works a little differently.

Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy enforcement.

Given the same example list from above:

    1-3 a: abcde is valid: position 1 contains a and position 3 does not.
    1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
    2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.

How many passwords are valid according to the new interpretation of the policies? */

:- use_module(library(dcg/basics), [integer//1, whites//0, string_without//2]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(apply), [include/3]).
:- use_module(library(lists), [nth1/3]).

password(pw(Min, Max, L, Pass)) -->
    integer(Min), "-", integer(Max),
    whites, [L], ":", whites, string_without("\n", Pass).

passwords([Pass|Passes]) -->
    password(Pass), "\n", !,
    passwords(Passes).
passwords([]) --> [].

code_count(C, Cs, N) :-
    code_count(Cs, C, 0, N).
code_count([], _, N, N).
code_count([C|Cs], C, N0, N) :- !,
    succ(N0, N1),
    code_count(Cs, C, N1, N).
code_count([_|Cs], C, N0, N) :-
    code_count(Cs, C, N0, N).

password_valid(pw(Min, Max, L, Pass)) :-
    code_count(L, Pass, N),
    between(Min, Max, N).

solve1(Input, Answer) :-
    phrase_from_file(passwords(Passes), Input),
    include(password_valid, Passes, ValidPasses),
    length(ValidPasses, NValid),
    Answer = NValid.

password_valid2(pw(N1, N2, L, Pass)) :-
    nth1(N1, Pass, L),
    nth1(N2, Pass, O), O \= L.
password_valid2(pw(N1, N2, L, Pass)) :-
    nth1(N2, Pass, L),
    nth1(N1, Pass, O), O \= L.

solve2(Input, Answer) :-
    phrase_from_file(passwords(Passes), Input),
    include(password_valid2, Passes, ValidPasses),
    length(ValidPasses, NValid),
    Answer = NValid.
