:- module(p18, []).
/*
--- Day 18: Operation Order ---

As you look out the window and notice a heavily-forested continent slowly appear over the horizon, you are interrupted by the child sitting next to you. They're curious if you could help them with their math homework.

Unfortunately, it seems like this "math" follows different rules than you remember.

The homework (your puzzle input) consists of a series of expressions that consist of addition (+), multiplication (*), and parentheses ((...)). Just like normal math, parentheses indicate that the expression inside must be evaluated before it can be used by the surrounding expression. Addition still finds the sum of the numbers on both sides of the operator, and multiplication still finds the product.

However, the rules of operator precedence have changed. Rather than evaluating multiplication before addition, the operators have the same precedence, and are evaluated left-to-right regardless of the order in which they appear.

For example, the steps to evaluate the expression 1 + 2 * 3 + 4 * 5 + 6 are as follows:

1 + 2 * 3 + 4 * 5 + 6
  3   * 3 + 4 * 5 + 6
      9   + 4 * 5 + 6
         13   * 5 + 6
             65   + 6
                 71

Parentheses can override this order; for example, here is what happens if parentheses are added to form 1 + (2 * 3) + (4 * (5 + 6)):

1 + (2 * 3) + (4 * (5 + 6))
1 +    6    + (4 * (5 + 6))
     7      + (4 * (5 + 6))
     7      + (4 *   11   )
     7      +     44
            51

Here are a few more examples:

    2 * 3 + (4 * 5) becomes 26.
    5 + (8 * 3 + 9 + 3 * 4 * 3) becomes 437.
    5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) becomes 12240.
    ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 becomes 13632.

Before you can help with the homework, you need to understand it yourself. Evaluate the expression on each line of the homework; what is the sum of the resulting values?

--- Part Two ---

You manage to answer the child's questions and they finish part 1 of their homework, but get stuck when they reach the next section: advanced math.

Now, addition and multiplication have different precedence levels, but they're not the ones you're familiar with. Instead, addition is evaluated before multiplication.

For example, the steps to evaluate the expression 1 + 2 * 3 + 4 * 5 + 6 are now as follows:

1 + 2 * 3 + 4 * 5 + 6
  3   * 3 + 4 * 5 + 6
  3   *   7   * 5 + 6
  3   *   7   *  11
     21       *  11
         231

Here are the other examples from above:

    1 + (2 * 3) + (4 * (5 + 6)) still becomes 51.
    2 * 3 + (4 * 5) becomes 46.
    5 + (8 * 3 + 9 + 3 * 4 * 3) becomes 1445.
    5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) becomes 669060.
    ((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 becomes 23340.

What do you get if you add up the results of evaluating the homework problems using these new rules? */

:- use_module(library(apply), [maplist/4, maplist/3, foldl/4, exclude/3, include/3, convlist/3]).
:- use_module(library(aggregate), [aggregate_all/3, foreach/2]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, whites//0, integer//1]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [select/3, member/2, permutation/2, nth1/3, sum_list/2]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(readutil), [read_file_to_string/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(thread), [concurrent_forall/2]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).

oper('*') --> "*".
oper('+') --> "+".

:- table expr//1.

expr(expr(Op, L, R)) -->
    expr(L), whites, oper(Op), whites, term(R).
expr(L) --> term(L).

term(I) --> integer(I).
term(Ex) -->
    "(", whites, expr(Ex), whites, ")".

solve1(Input, Answer) :-
    abolish_module_tables(p18),
    read_file_to_string(Input, Contents, []),
    split_string(Contents, "\n", "", Lines),
    convlist([Line, Expr]>>(
                 Line \== "",
                 string_codes(Line, Codes),
                 phrase(expr(Expr), Codes)
             ), Lines, Exprs),
    maplist(eval_expr, Exprs, Results),
    sum_list(Results, Answer).

eval_expr(expr('*', L, R), A) :- !,
    eval_expr(L, La), eval_expr(R, Ra),
    A is La * Ra.
eval_expr(expr('+', L, R), A) :- !,
    eval_expr(L, La), eval_expr(R, Ra),
    A is La + Ra.
eval_expr(I, I) :- number(I).

solve2(Input, Answer) :-
    abolish_module_tables(p18),
    read_file_to_string(Input, Contents, []),
    split_string(Contents, "\n", "", Lines),
    convlist([Line, Expr]>>(
                 Line \== "",
                 string_codes(Line, Codes),
                 phrase(expr2(Expr), Codes)
             ), Lines, Exprs),
    maplist(eval_expr, Exprs, Results),
    % debug(xxx, "Results ~w", [Results]),
    sum_list(Results, Answer).

:- table expr2//1.
:- table factor//1.

expr2(expr('*', L, R)) -->
    expr2(L), whites, "*", whites, factor(R).
expr2(L) --> factor(L).

factor(expr('+', L, R)) -->
    factor(L), whites, "+", whites, term2(R).
factor(F) --> term2(F).

term2(I) --> integer(I).
term2(Ex) -->
    "(", whites, expr2(Ex), whites, ")".
