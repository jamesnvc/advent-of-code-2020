:- module(p21, []).
/*
--- Day 21: Allergen Assessment ---

You reach the train's last stop and the closest you can get to your vacation island without getting wet. There aren't even any boats here, but nothing can stop you now: you build a raft. You just need a few days' worth of food for your journey.

You don't speak the local language, so you can't read any ingredients lists. However, sometimes, allergens are listed in a language you do understand. You should be able to use this information to determine which ingredient contains which allergen and work out which foods are safe to take with you on your trip.

You start by compiling a list of foods (your puzzle input), one food per line. Each line includes that food's ingredients list followed by some or all of the allergens the food contains.

Each allergen is found in exactly one ingredient. Each ingredient contains zero or one allergen. Allergens aren't always marked; when they're listed (as in (contains nuts, shellfish) after an ingredients list), the ingredient that contains each listed allergen will be somewhere in the corresponding ingredients list. However, even if an allergen isn't listed, the ingredient that contains that allergen could still be present: maybe they forgot to label it, or maybe it was labeled in a language you don't know.

For example, consider the following list of foods:

mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)

The first food in the list has four ingredients (written in a language you don't understand): mxmxvkd, kfcds, sqjhc, and nhms. While the food might contain other allergens, a few allergens the food definitely contains are listed afterward: dairy and fish.

The first step is to determine which ingredients can't possibly contain any of the allergens in any food in your list. In the above example, none of the ingredients kfcds, nhms, sbzzf, or trh can contain an allergen. Counting the number of times any of these ingredients appear in any ingredients list produces 5: they all appear once each except sbzzf, which appears twice.

Determine which ingredients cannot possibly contain any of the allergens in your list. How many times do any of those ingredients appear?
--- Part Two ---

Now that you've isolated the inert ingredients, you should have enough information to figure out which ingredient contains which allergen.

In the above example:

    mxmxvkd contains dairy.
    sqjhc contains fish.
    fvjkl contains soy.

Arrange the ingredients alphabetically by their allergen and separate them by commas to produce your canonical dangerous ingredient list. (There should not be any spaces in your canonical dangerous ingredient list.) In the above example, this would be mxmxvkd,sqjhc,fvjkl.

Time to stock your raft with supplies. What is your canonical dangerous ingredient list?
*/

:- use_module(library(apply_macros)).
:- use_module(library(apply), [maplist/4, maplist/3, maplist/2, foldl/4, exclude/3, include/3, convlist/3]).
:- use_module(library(aggregate), [aggregate_all/3, foreach/2]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, whites//0, integer//1, eos//0]).
:- use_module(library(dcg/high_order), [sequence//2]).
:- use_module(library(ordsets), [list_to_ord_set/2, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [select/3, member/2, permutation/2, nth1/3, sum_list/2, last/2, append/2, append/3, reverse/2]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(readutil), [read_file_to_string/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(thread), [concurrent_forall/2]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).

ingredients_allergens(line(Ingredients, Allergens)) -->
    ingredients(Ingredients),
    "(contains ", allergens(Allergens), "\n".

ingredients([Ingredient|Ingredients]) -->
    string_without(" (", IngredientCodes),
    { string_codes(Ingredient, IngredientCodes) },
    " ",
    ingredients(Ingredients).
ingredients([]) --> [].

allergens([Allergen|Allergens]) -->
    string_without(",)", AllergenCodes),
    { string_codes(Allergen, AllergenCodes) },
    allergens_next(Allergens).
allergens_next([]) --> ")".
allergens_next(Allergens) -->
    ", ", allergens(Allergens).

ingredients_allergens_lines([Line|Lines]) -->
    ingredients_allergens(Line),
    ingredients_allergens_lines(Lines).
ingredients_allergens_lines([]) --> [].

allergen_ingredients(Lines, AIs) :-
    rb_empty(Mapping),
    allergen_ingredients(Lines, Mapping, AIs).

allergen_ingredients([], M, M).
allergen_ingredients([line(Ins0, Als)|Lines], M0, M) :-
    list_to_ord_set(Ins0, Ins),
    update_potential_allergens(Als, Ins, M0, M1),
    allergen_ingredients(Lines, M1, M).

update_potential_allergens([], _, M, M).
update_potential_allergens([A|As], Ins, M0, M) :-
    rb_apply(M0, A, ord_intersection(Ins), M1), !,
    update_potential_allergens(As, Ins, M1, M).
update_potential_allergens([A|As], Ins, M0, M) :-
    rb_insert(M0, A, Ins, M1),
    update_potential_allergens(As, Ins, M1, M).

solve1(Input, Answer) :-
    phrase_from_file(ingredients_allergens_lines(Lines), Input), !,
    foldl([line(Ins, Alls), I0-A0, I1-A1]>>(
              list_to_ord_set(Ins, InsSet),
              ord_union(I0, InsSet, I1),
              list_to_ord_set(Alls, AllsSet),
              ord_union(A0, AllsSet, A1)
          ),
          Lines,
          []-[],
          AllIngredients-_AllAllergens
         ),
    allergen_ingredients(Lines, AllergenIngredients),
    rb_fold([_-Ins, S0, S1]>>ord_union(S0, Ins, S1),
            AllergenIngredients,
            [],
            PossibleIngredients),
    ord_subtract(AllIngredients, PossibleIngredients, NoAllergenIngredients),
    aggregate_all(count,
                  ( member(Ingredient, NoAllergenIngredients),
                    member(line(Ingredients, _), Lines),
                    member(Ingredient, Ingredients) ),
                  Answer).

solve_ingredient_allergens(AIs, IAs) :-
    setof(Al-In, ( rb_in(_, Ins, AIs), length(Ins, L), L > 1 ), _),
    !,
    setof(Al-In, rb_in(Al, [In], AIs), SingleIngredients),
    foldl([Al-In, M0, M1]>>(
              rb_map(M0, maybe_element_remove(In), M1)
          ),
          SingleIngredients, AIs, AIs1),
    solve_ingredient_allergens(AIs1, IAs).
solve_ingredient_allergens(AIs, IAs) :-
    findall(In-Al, rb_in(Al, [In], AIs), IAs).

maybe_element_remove(El, [El], [El]) :- !.
maybe_element_remove(El, Ord0, Ord1) :-
    ord_del_element(Ord0, El, Ord1).

solve2(Input, Answer) :-
    phrase_from_file(ingredients_allergens_lines(Lines), Input), !,
    allergen_ingredients(Lines, AllergenIngredients),
    solve_ingredient_allergens(AllergenIngredients, IngredientAllergens),
    sort(2, @=<, IngredientAllergens, SortedInAls),
    maplist([In-_, In]>>true, SortedInAls, SortedIns),
    atomic_list_concat(SortedIns, ",", Answer).
