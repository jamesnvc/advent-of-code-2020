:- module(p20, []).
/* --- Day 20: Jurassic Jigsaw ---

The high-speed train leaves the forest and quickly carries you south. You can even see a desert in the distance! Since you have some spare time, you might as well see if there was anything interesting in the image the Mythical Information Bureau satellite captured.

After decoding the satellite messages, you discover that the data actually contains many small images created by the satellite's camera array. The camera array consists of many cameras; rather than produce a single square image, they produce many smaller square image tiles that need to be reassembled back into a single image.

Each camera in the camera array returns a single monochrome image tile with a random unique ID number. The tiles (your puzzle input) arrived in a random order.

Worse yet, the camera array appears to be malfunctioning: each image tile has been rotated and flipped to a random orientation. Your first task is to reassemble the original image by orienting the tiles so they fit together.

To show how the tiles should be reassembled, each tile's image data includes a border that should line up exactly with its adjacent tiles. All tiles have this border, and the border lines up exactly when the tiles are both oriented correctly. Tiles at the edge of the image also have this border, but the outermost edges won't line up with any other tiles.

For example, suppose you have the following nine tiles:

Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...

By rotating, flipping, and rearranging them, you can find a square arrangement that causes all adjacent borders to line up:

#...##.#.. ..###..### #.#.#####.
..#.#..#.# ###...#.#. .#..######
.###....#. ..#....#.. ..#.......
###.##.##. .#.#.#..## ######....
.###.##### ##...#.### ####.#..#.
.##.#....# ##.##.###. .#...#.##.
#...###### ####.#...# #.#####.##
.....#..## #...##..#. ..#.###...
#.####...# ##..#..... ..#.......
#.##...##. ..##.#..#. ..#.###...

#.##...##. ..##.#..#. ..#.###...
##..#.##.. ..#..###.# ##.##....#
##.####... .#.####.#. ..#.###..#
####.#.#.. ...#.##### ###.#..###
.#.####... ...##..##. .######.##
.##..##.#. ....#...## #.#.#.#...
....#..#.# #.#.#.##.# #.###.###.
..#.#..... .#.##.#..# #.###.##..
####.#.... .#..#.##.. .######...
...#.#.#.# ###.##.#.. .##...####

...#.#.#.# ###.##.#.. .##...####
..#.#.###. ..##.##.## #..#.##..#
..####.### ##.#...##. .#.#..#.##
#..#.#..#. ...#.#.#.. .####.###.
.#..####.# #..#.#.#.# ####.###..
.#####..## #####...#. .##....##.
##.##..#.. ..#...#... .####...#.
#.#.###... .##..##... .####.##.#
#...###... ..##...#.. ...#..####
..#.#....# ##.#.#.... ...##.....

For reference, the IDs of the above tiles are:

1951    2311    3079
2729    1427    2473
2971    1489    1171

To check that you've assembled the image correctly, multiply the IDs of the four corner tiles together. If you do this with the assembled tiles from the example above, you get 1951 * 3079 * 2971 * 1171 = 20899048083289.

Assemble the tiles into an image. What do you get if you multiply together the IDs of the four corner tiles?

--- Part Two ---

Now, you're ready to check the image for sea monsters.

The borders of each tile are not part of the actual image; start by removing them.

In the example above, the tiles become:

.#.#..#. ##...#.# #..#####
###....# .#....#. .#......
##.##.## #.#.#..# #####...
###.#### #...#.## ###.#..#
##.#.... #.##.### #...#.##
...##### ###.#... .#####.#
....#..# ...##..# .#.###..
.####... #..#.... .#......

#..#.##. .#..###. #.##....
#.####.. #.####.# .#.###..
###.#.#. ..#.#### ##.#..##
#.####.. ..##..## ######.#
##..##.# ...#...# .#.#.#..
...#..#. .#.#.##. .###.###
.#.#.... #.##.#.. .###.##.
###.#... #..#.##. ######..

.#.#.### .##.##.# ..#.##..
.####.## #.#...## #.#..#.#
..#.#..# ..#.#.#. ####.###
#..####. ..#.#.#. ###.###.
#####..# ####...# ##....##
#.##..#. .#...#.. ####...#
.#.###.. ##..##.. ####.##.
...###.. .##...#. ..#..###

Remove the gaps to form the actual image:

.#.#..#.##...#.##..#####
###....#.#....#..#......
##.##.###.#.#..######...
###.#####...#.#####.#..#
##.#....#.##.####...#.##
...########.#....#####.#
....#..#...##..#.#.###..
.####...#..#.....#......
#..#.##..#..###.#.##....
#.####..#.####.#.#.###..
###.#.#...#.######.#..##
#.####....##..########.#
##..##.#...#...#.#.#.#..
...#..#..#.#.##..###.###
.#.#....#.##.#...###.##.
###.#...#..#.##.######..
.#.#.###.##.##.#..#.##..
.####.###.#...###.#..#.#
..#.#..#..#.#.#.####.###
#..####...#.#.#.###.###.
#####..#####...###....##
#.##..#..#...#..####...#
.#.###..##..##..####.##.
...###...##...#...#..###

Now, you're ready to search for sea monsters! Because your image is monochrome, a sea monster will look like this:

                  #
#    ##    ##    ###
 #  #  #  #  #  #

When looking for this pattern in the image, the spaces can be anything; only the # need to match. Also, you might need to rotate or flip your image before it's oriented correctly to find sea monsters. In the above image, after flipping and rotating it to the appropriate orientation, there are two sea monsters (marked with O):

.####...#####..#...###..
#####..#..#.#.####..#.#.
.#.#...#.###...#.##.O#..
#.O.##.OO#.#.OO.##.OOO##
..#O.#O#.O##O..O.#O##.##
...#.#..##.##...#..#..##
#.##.#..#.#..#..##.#.#..
.###.##.....#...###.#...
#.####.#.#....##.#..#.#.
##...#..#....#..#...####
..#.##...###..#.#####..#
....#.##.#.#####....#...
..##.##.###.....#.##..#.
#...#...###..####....##.
.#.##...#.##.#.#.###...#
#.###.#..####...##..#...
#.###...#.##...#.##O###.
.O##.#OO.###OO##..OOO##.
..O#.O..O..O.#O##O##.###
#.#..##.########..#..##.
#.#####..#.#...##..#....
#....##..#.#########..##
#...#.....#..##...###.##
#..###....##.#...##.##.#

Determine how rough the waters are in the sea monsters' habitat by counting the number of # that are not part of a sea monster. In the above example, the habitat's water roughness is 273.

How many # are not part of a sea monster? */

:- use_module(library(apply_macros)).
:- use_module(library(apply), [maplist/4, maplist/3, maplist/2, foldl/4, exclude/3, include/3, convlist/3]).
:- use_module(library(aggregate), [aggregate_all/3, foreach/2]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, whites//0, integer//1, eos//0]).
:- use_module(library(dcg/high_order), [sequence//2]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [select/3, member/2, permutation/2, nth1/3, sum_list/2, last/2, append/2, append/3, reverse/2]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(readutil), [read_file_to_string/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(thread), [concurrent_forall/2]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).
:- use_module(library(reif), [if_/3, (=)/3]).


tile(tile(Id, Data)) -->
    "Tile ", integer(Id), ":\n",
    tile_lines(Data).

tile_lines([]) --> "\n".
tile_lines([]) --> eos.
tile_lines([Line|Lines]) -->
    sequence(line_char, Line), "\n",
    tile_lines(Lines).

line_char('#') --> "#".
line_char('.') --> ".".

tiles([T|Tiles]) -->
    tile(T), !,
    tiles(Tiles).
tiles([]) --> [].

:- dynamic bit_length/1.

solve1(Input, Answer) :-
    retractall(bit_length(_)),
    phrase_from_file(tiles(Tiles), Input),
    Tiles = [tile(_, [F|_])|_],
    length(F, BitLen),
    assertz(bit_length(BitLen)),
    maplist(encode_tile_edges, Tiles, CodedTiles),
    tiles_permutations(CodedTiles, TilePermutations),
    length(Tiles, NTiles),
    SideLen is floor(sqrt(NTiles)),
    length(Grid, SideLen),
    maplist({SideLen}/[Line]>>length(Line, SideLen), Grid),
    fill_grid(Grid, TilePermutations),
    nth1(1, Grid, TopRow), nth1(SideLen, Grid, BottomRow),
    nth1(1, TopRow, tile(C1, _, _)), nth1(SideLen, TopRow, tile(C2, _, _)),
    nth1(1, BottomRow, tile(C3, _, _)), nth1(SideLen, BottomRow, tile(C4, _, _)),
    Answer is C1 * C2 * C3 * C4.

encode_edge(Chars, Ident) :-
    foldl([C, I0, I1]>>(
              char_val(C, V),
              I1 is I0 << 1 \/ V
          ),
          Chars, 0, Ident).

char_val('.', 0).
char_val('#', 1).

encode_tile_edges(tile(Id, Data), tile(Id, [T, R, L, B])) :-
    once(append([Tc|_], [Bc], Data)),
    encode_edge(Tc, T), encode_edge(Bc, B),
    maplist([[C|_], C]>>true, Data, Lc),
    maplist(last, Data, Rc),
    encode_edge(Rc, R), encode_edge(Lc, L).

bit_reverse(N, Nr) :-
    bit_length(NBits),
    bit_reverse(NBits, N, 0, Nr).
bit_reverse(0, _, N, N) :- !.
bit_reverse(NBits, N0, Nr0, Nr) :-
    Nr1 is (Nr0 << 1) \/ (N0 /\ 1),
    N1 is N0 >> 1,
    NBits0 is NBits - 1,
    bit_reverse(NBits0, N1, Nr1, Nr).

transform_tile(identity, Tile, Tile).
transform_tile(flip,
               tile(Id, [T, R, L, B]),
               tile(Id, [Tr, L, R, Br])
              ) :-
    bit_reverse(T, Tr),
    bit_reverse(B, Br).
transform_tile(rotate,
               tile(Id, [T, R, L, B]),
               tile(Id, [Lr, T, B, Rr])
              ) :-
    bit_reverse(L, Lr), bit_reverse(R, Rr).
transform_tile(rotate2) -->
    transform_tile(rotate),
    transform_tile(rotate).
transform_tile(rotate3) -->
    transform_tile(rotate2),
    transform_tile(rotate).
transform_tile(flip_rotate) -->
    transform_tile(flip),
    transform_tile(rotate).
transform_tile(flip_rotate2) -->
    transform_tile(flip),
    transform_tile(rotate2).
transform_tile(flip_rotate3) -->
    transform_tile(flip),
    transform_tile(rotate3).

tile_permutations(Tile, Permuts) :-
    findall(tile(Id, Sides, Method),
            transform_tile(Method, Tile, tile(Id, Sides)),
            Permuts).

tiles_permutations([], []).
tiles_permutations([Tile|Ts], [P1,P2,P3,P4,P5,P6,P7,P8|Ps]) :-
    tile_permutations(Tile, [P1,P2,P3,P4,P5,P6,P7,P8]),
    tiles_permutations(Ts, Ps).

remove_tile(Id, Tiles0, Tiles1) :-
    exclude({Id}/[tile(Id, _, _)]>>true, Tiles0, Tiles1).

select_tile(Tile, Tiles0, Tiles1) :-
    member(Tile, Tiles0),
    Tile = tile(Id, _, _),
    remove_tile(Id, Tiles0, Tiles1).

tile_dne(Tile, Tiles) :-
    \+ memberchk(Tile, Tiles).

fill_grid(Grid, Tiles) :-
    length(Grid, L),
    fill_grid(L, L, Tiles, Grid, L).
fill_grid(0, _, _, _, _) :- !.
fill_grid(I, J, Tiles, Grid, SideLen) :-
    tile_fits(Tile, I, J, Grid),
    select_tile(Tile, Tiles, Tiles1),
    nth1(I, Grid, Row), nth1(J, Row, Tile),
    next_idx(SideLen, I, J, I1, J1),
    fill_grid(I1, J1, Tiles1, Grid, SideLen).

tile_fits(tile(_, [_, E, _, S], _), I, J, Grid) :-
    length(Grid, Len),
    if_(J = Len,
        true,
        ( J1 is J + 1,
          nth1(I, Grid, RowJ),
          nth1(J1, RowJ, Te),
          Te = tile(_, [_, _, E, _], _)
        )),
    if_(I = Len,
        true,
        ( I1 is I + 1,
          nth1(I1, Grid, RowI),
          nth1(J, RowI, Ts),
          Ts = tile(_, [S, _, _, _], _)
        ) ).

next_idx(L, I, 1, I1, L) :-
    !, I1 is I - 1.
next_idx(_, I, J, I, J1) :-
    J1 is J - 1.

solve2(Input, Answer) :-
    retractall(bit_length(_)),
    phrase_from_file(tiles(Tiles), Input),
    Tiles = [tile(_, [F|_])|_],
    length(F, BitLen),
    assertz(bit_length(BitLen)),
    maplist(encode_tile_edges, Tiles, CodedTiles),
    tiles_permutations(CodedTiles, TilePermutations),
    length(Tiles, NTiles),
    SideLen is floor(sqrt(NTiles)),
    length(Grid, SideLen),
    maplist({SideLen}/[Line]>>length(Line, SideLen), Grid),
    once(fill_grid(Grid, TilePermutations)),
    build_real_grid(Grid, Tiles, FullGrid),
    aggregate_all(sum(NSerpents),
                  ( transform_grid(_Trans, FullGrid, TGrid),
                    n_seaserpents(TGrid, NSerpents)
                  ),
                  TotalSerpents
                 ),
    aggregate_all(count,
                  ( member(Row, FullGrid),
                    member('#', Row)
                  ),
                  TotalCells),
    Answer is TotalCells - (TotalSerpents * 15).

n_seaserpents(Grid, N) :-
    length(Grid, GridLen),
    aggregate_all(count,
                  ( between(1, GridLen, I),
                    between(1, GridLen, J),
                    sea_serpent_at(I, J, Grid)
                  ),
                  N).

build_real_grid([], _, []).
build_real_grid([Row|Rows], Tiles, FullRows) :-
    build_real_row(Row, FullRow, Tiles),
    append(FullRow, FullRowsTail, FullRows),
    build_real_grid(Rows, Tiles, FullRowsTail).

build_real_row(TileRow, FullRow, Tiles) :-
    maplist(tile_grid(Tiles), TileRow, FullRow0),
    join_grids(FullRow0, FullRow).

join_grids(Grids, [GridRow|GridRows]) :-
    maplist([[R|Rs], R, Rs]>>true, Grids, Firsts, Rests), !,
    append(Firsts, GridRow),
    join_grids(Rests, GridRows).
join_grids(Empties, []) :- maplist(==([]), Empties).

tile_grid(Tiles, tile(Id, _, Transform), RealGrid) :-
    memberchk(tile(Id, Grid), Tiles),
    transform_grid(Transform, Grid, RealGrid0),
    inner(RealGrid0, RealGrid).

transform_grid(identity, Grid, Grid).
transform_grid(flip, Grid, GridFlip) :-
    maplist(reverse, Grid, GridFlip).
transform_grid(rotate, Grid, GridRot) :-
    transpose(Grid, GridT),
    maplist(reverse, GridT, GridRot).
transform_grid(rotate2) -->
    transform_grid(rotate),
    transform_grid(rotate).
transform_grid(rotate3) -->
    transform_grid(rotate2),
    transform_grid(rotate).
transform_grid(flip_rotate) -->
    transform_grid(flip),
    transform_grid(rotate).
transform_grid(flip_rotate2) -->
    transform_grid(flip),
    transform_grid(rotate2).
transform_grid(flip_rotate3) -->
    transform_grid(flip),
    transform_grid(rotate3).

inner(Grid, Grid1) :-
    middle(Grid, GridMiddle),
    maplist(middle, GridMiddle, Grid1).

middle([_|Tail], Middle) :-
    once(append(Middle, [_], Tail)).

% I, J = "tail" of serpent
sea_serpent_at(I, J, Grid) :-
    I0 is I - 1, I1 is I + 1,
    nth1(I0, Grid, TopRow),
    nth1(I, Grid, MidRow),
    nth1(I1, Grid, BottomRow),

    J2 is J + 1,
    nth1(J, MidRow, '#'),
    nth1(J2, BottomRow, '#'),

    J3 is J + 4,
    J4 is J + 5,
    J5 is J + 6,
    J6 is J + 7,
    nth1(J3, BottomRow, '#'),
    nth1(J4, MidRow, '#'),
    nth1(J5, MidRow, '#'),
    nth1(J6, BottomRow, '#'),

    J7 is J + 10,
    J8 is J + 11,
    J9 is J + 12,
    J10 is J + 13,
    nth1(J7, BottomRow, '#'),
    nth1(J8, MidRow, '#'),
    nth1(J9, MidRow, '#'),
    nth1(J10, BottomRow, '#'),

    J11 is J + 16,
    J12 is J + 17,
    J13 is J + 18,
    J14 is J + 19,

    nth1(J11, BottomRow, '#'),
    nth1(J12, MidRow, '#'),
    nth1(J13, MidRow, '#'),
    nth1(J13, TopRow, '#'),
    nth1(J14, MidRow, '#').

test :-
    solve2('test_20.txt', A),
    maplist([R]>>(atomic_list_concat(R, '', L), writeln(L)), A).
