:- module(p16, []).
/*
--- Day 16: Ticket Translation ---

As you're walking to yet another connecting flight, you realize that one of the legs of your re-routed trip coming up is on a high-speed train. However, the train ticket you were given is in a language you don't understand. You should probably figure out what it says before you get to the train station after the next flight.

Unfortunately, you can't actually read the words on the ticket. You can, however, read the numbers, and so you figure out the fields these tickets must have and the valid ranges for values in those fields.

You collect the rules for ticket fields, the numbers on your ticket, and the numbers on other nearby tickets for the same train service (via the airport security cameras) together into a single document you can reference (your puzzle input).

The rules for ticket fields specify a list of fields that exist somewhere on the ticket and the valid ranges of values for each field. For example, a rule like class: 1-3 or 5-7 means that one of the fields in every ticket is named class and can be any value in the ranges 1-3 or 5-7 (inclusive, such that 3 and 5 are both valid in this field, but 4 is not).

Each ticket is represented by a single line of comma-separated values. The values are the numbers on the ticket in the order they appear; every ticket has the same format. For example, consider this ticket:

.--------------------------------------------------------.
| ????: 101    ?????: 102   ??????????: 103     ???: 104 |
|                                                        |
| ??: 301  ??: 302             ???????: 303      ??????? |
| ??: 401  ??: 402           ???? ????: 403    ????????? |
'--------------------------------------------------------'

Here, ? represents text in a language you don't understand. This ticket might be represented as 101,102,103,104,301,302,303,401,402,403; of course, the actual train tickets you're looking at are much more complicated. In any case, you've extracted just the numbers in such a way that the first number is always the same specific field, the second number is always a different specific field, and so on - you just don't know what each position actually means!

Start by determining which tickets are completely invalid; these are tickets that contain values which aren't valid for any field. Ignore your ticket for now.

For example, suppose you have the following notes:

class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12

It doesn't matter which position corresponds to which field; you can identify invalid nearby tickets by considering only whether tickets contain values that are not valid for any field. In this example, the values on the first nearby ticket are all valid for at least one field. This is not true of the other three nearby tickets: the values 4, 55, and 12 are are not valid for any field. Adding together all of the invalid values produces your ticket scanning error rate: 4 + 55 + 12 = 71.

Consider the validity of the nearby tickets you scanned. What is your ticket scanning error rate? */

:- use_module(library(apply), [maplist/4, maplist/3, foldl/4, exclude/3, include/3]).
:- use_module(library(aggregate), [aggregate_all/3, foreach/2]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, integer//1]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [select/3, member/2, permutation/2, nth1/3, sum_list/2]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).


rule(rule(Name, Ranges)) -->
    string_without(":", NameC), ": ",
    { string_codes(Name, NameC) },
    rule_ranges(Ranges), "\n".

rule_ranges([range(R0l, R0h), range(R1l, R1h)]) -->
    integer(R0l), "-", integer(R0h),
    " or ",
    integer(R1l), "-", integer(R1h).


rules([Rule|Rules]) -->
    rule(Rule), !, next_rules(Rules).
next_rules([]) --> "\n".
next_rules(Rules) --> rules(Rules).

ticket(Ns) -->
    string_without("\n", Line),
    { split_string(Line, ",", "", NStrs),
      maplist(number_string, Ns, NStrs) },
    "\n".

tickets([]) --> [].
tickets([T|Ts]) --> ticket(T), tickets(Ts).

puzzle_input(input(Rules, Ticket, Nearby)) -->
    rules(Rules),
    "your ticket:\n",
    ticket(Ticket),
    "\n",
    "nearby tickets:\n",
    tickets(Nearby).

solve1(Input, Answer) :-
    phrase_from_file(puzzle_input(input(Rules, _, Nearby)), Input), !,
    count_invalid_tickets(Nearby, Rules, 0, Answer).

count_invalid_tickets([], _, N, N).
count_invalid_tickets([T|Ts], Rules, N0, N) :-
    invalid_fields(T, Rules, Fs),
    N1 is N0 + Fs,
    count_invalid_tickets(Ts, Rules, N1, N).

invalid_fields(T, Rules, Fs) :-
    exclude(fits_range(Rules), T, Invalid),
    sum_list(Invalid, Fs).

valid_ticket(Rules, Ticket) :-
    forall(member(T, Ticket), fits_range(Rules, T)).

fits_range(Rules, N) :-
    member(rule(_, Ranges), Rules),
    member(range(Lo, Hi), Ranges),
    between(Lo, Hi, N).

solve2(Input, Answer) :-
    phrase_from_file(puzzle_input(input(Rules, MyTicket, Nearby)), Input), !,
    include(valid_ticket(Rules), Nearby, ValidTickets),
    find_rule_ordering(Rules, ValidTickets, OrderedRules),
    debug(xxx, "DONE ordered rules ~w", [OrderedRules]),
    !,
    findall(Field,
            ( nth1(I, OrderedRules, rule(Rule, _)),
              string_concat("departure", _, Rule),
              nth1(I, MyTicket, Field)
            ),
            DepartureFields),
    foldl([X,Y,Z]>>(Z is X * Y), DepartureFields, 1, Answer).

find_rule_ordering(Rules, Tickets, OrderedRules) :-
    find_rule_ordering(Rules, [], Tickets, ValidRuleIdxs),
    maplist([Valids-R, R-Idx]>>(
                xfy_list('\\/', Indices, Valids),
                Idx in Indices
            ),
            ValidRuleIdxs, RuleIdxs),
    maplist([_-Idx, Idx]>>true, RuleIdxs, Indices),
    all_distinct(Indices),
    labeling([ffc], Indices),
    sort(2, @=<, RuleIdxs, Ordered0),
    maplist([R-_, R]>>true, Ordered0, OrderedRules).

find_rule_ordering([], ValidRuleIdxs, _, ValidRuleIdxs).
find_rule_ordering([Rule|Rules], Rules0, Tickets, ValidRuleIdxs) :-
    length(Tickets, NTickets),
    setof(Idx,
          ( between(1, NTickets, Idx),
            check_rule_field(Rule, Idx, Tickets) ),
          RuleIdxs
         ),
    find_rule_ordering(Rules, [RuleIdxs-Rule|Rules0], Tickets, ValidRuleIdxs).

check_rule_field(rule(_, Ranges), I, ValidTickets) :-
    maplist(nth1(I), ValidTickets, Fields),
    forall(
        member(Field, Fields),
        ( member(range(Lo, Hi), Ranges),
          between(Lo, Hi, Field)
        )
    ).

xfy_list(Op, Term, [Left|List]) :-
    Term =.. [Op, Left, Right],
    xfy_list(Op, Right, List),
    !.
xfy_list(_, Term, [Term]).
