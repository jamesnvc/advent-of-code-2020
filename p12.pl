:- module(p12, []).
/*
--- Day 12: Rain Risk ---

Your ferry made decent progress toward the island, but the storm came in faster than anyone expected. The ferry needs to take evasive actions!

Unfortunately, the ship's navigation computer seems to be malfunctioning; rather than giving a route directly to safety, it produced extremely circuitous instructions. When the captain uses the PA system to ask if anyone can help, you quickly volunteer.

The navigation instructions (your puzzle input) consists of a sequence of single-character actions paired with integer input values. After staring at them for a few minutes, you work out what they probably mean:

    Action N means to move north by the given value.
    Action S means to move south by the given value.
    Action E means to move east by the given value.
    Action W means to move west by the given value.
    Action L means to turn left the given number of degrees.
    Action R means to turn right the given number of degrees.
    Action F means to move forward by the given value in the direction the ship is currently facing.

The ship starts by facing east. Only the L and R actions change the direction the ship is facing. (That is, if the ship is facing east and the next instruction is N10, the ship would move north 10 units, but would still move east if the following action were F.)

For example:

F10
N3
F7
R90
F11

These instructions would be handled as follows:

    F10 would move the ship 10 units east (because the ship starts by facing east) to east 10, north 0.
    N3 would move the ship 3 units north to east 10, north 3.
    F7 would move the ship another 7 units east (because the ship is still facing east) to east 17, north 3.
    R90 would cause the ship to turn right by 90 degrees and face south; it remains at east 17, north 3.
    F11 would move the ship 11 units south to east 17, south 8.

At the end of these instructions, the ship's Manhattan distance (sum of the absolute values of its east/west position and its north/south position) from its starting position is 17 + 8 = 25.

Figure out where the navigation instructions lead. What is the Manhattan distance between that location and the ship's starting position? */


:- use_module(library(apply), [maplist/4, maplist/3, foldl/4]).
:- use_module(library(aggregate), [aggregate_all/3]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, integer//1]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [last/2, member/2, nth0/3]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).

instruction(forward(N)) --> "F", integer(N), "\n".
instruction(north(N)) --> "N", integer(N), "\n".
instruction(south(N)) --> "S", integer(N), "\n".
instruction(east(N)) --> "E", integer(N), "\n".
instruction(west(N)) --> "W", integer(N), "\n".
instruction(left(D)) --> "L", integer(D), "\n".
instruction(right(D)) --> "R", integer(D), "\n".

instructions([I|Is]) --> instruction(I), !, instructions(Is).
instructions([]) --> [].

solve1(Input, Answer) :-
    phrase_from_file(instructions(Insts), Input),
    interpret_instructions(Insts, state(0, 0, east), state(X, Y, _)),
    Answer is abs(X) + abs(Y).

interpret_instructions([Inst|Insts]) -->
    interpret_instruction(Inst), !,
    interpret_instructions(Insts).
interpret_instructions([], State, State).

interpret_instruction(forward(N), state(X0, Y0, Face), state(X1, Y1, Face)) :-
    advance_direction(Face, N, X0-Y0, X1-Y1).
interpret_instruction(north(N), state(X, Y0, Face), state(X, Y1, Face)) :-
    advance_direction(north, N, _-Y0, _-Y1).
interpret_instruction(south(N), state(X, Y0, Face), state(X, Y1, Face)) :-
    advance_direction(south, N, _-Y0, _-Y1).
interpret_instruction(east(N), state(X0, Y, Face), state(X1, Y, Face)) :-
    advance_direction(east, N, X0-_, X1-_).
interpret_instruction(west(N), state(X0, Y, Face), state(X1, Y, Face)) :-
    advance_direction(west, N, X0-_, X1-_).
interpret_instruction(right(D), state(X, Y, Face0), state(X, Y, Face1)) :-
    rotate(Face0, D, Face1).
interpret_instruction(left(D0), state(X, Y, Face0), state(X, Y, Face1)) :-
    D is -D0,
    rotate(Face0, D, Face1).

advance_direction(north, N, X-Y0, X-Y1) :- Y1 is Y0 + N.
advance_direction(south, N, X-Y0, X-Y1) :- Y1 is Y0 - N.
advance_direction(east, N, X0-Y, X1-Y) :- X1 is X0 + N.
advance_direction(west, N, X0-Y, X1-Y) :- X1 is X0 - N.

rotate(Face0, D, Face1) :-
    Rots is D / 90,
    nth0(I0, [north, east, south, west], Face0),
    I1 is (I0 + Rots) mod 4,
    nth0(I1, [north, east, south, west], Face1).

solve2(Input, Answer) :-
    phrase_from_file(instructions(Insts), Input),
    interpret_instructions2(Insts,
                            state(ship(0, 0), way(1, 10)),
                            state(ship(X, Y), way(_N, _E))),
    Answer is abs(X) + abs(Y).

interpret_instructions2([Inst|Insts]) -->
    interpret_instruction2(Inst), !,
    interpret_instructions2(Insts).
interpret_instructions2([], State, State).

interpret_instruction2(forward(A),
                       state(ship(X0, Y0), way(N, E)),
                       state(ship(X1, Y1), way(N, E))) :-
    X1 is X0 + E * A,
    Y1 is Y0 + N * A.
interpret_instruction2(north(A),
                       state(Ship, way(N0, E)),
                       state(Ship, way(N1, E))) :-
    N1 is N0 + A.
interpret_instruction2(south(A),
                       state(Ship, way(N0, E)),
                       state(Ship, way(N1, E))) :-
    N1 is N0 - A.
interpret_instruction2(east(A),
                       state(Ship, way(N, E0)),
                       state(Ship, way(N, E1))) :-
    E1 is E0 + A.
interpret_instruction2(west(A),
                       state(Ship, way(N, E0)),
                       state(Ship, way(N, E1))) :-
    E1 is E0 - A.
interpret_instruction2(left(D),
                       state(Ship, way(N0, E0)),
                       state(Ship, way(N1, E1))) :-
    rotate2(D, N0-E0, N1-E1).
interpret_instruction2(right(D0),
                       state(Ship, way(N0, E0)),
                       state(Ship, way(N1, E1))) :-
    D is D0 * -1,
    rotate2(D, N0-E0, N1-E1).

rotate2(Deg, Y0-X0, Y1-X1) :-
    Rad is Deg / 180 * pi,
    X1 is round(X0 * cos(Rad) - Y0 * sin(Rad)),
    Y1 is round(X0 * sin(Rad) + Y0 * cos(Rad)).
