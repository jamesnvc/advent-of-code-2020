:- module(p11, []).
/*
 --- Day 11: Seating System ---

Your plane lands with plenty of time to spare. The final leg of your journey is a ferry that goes directly to the tropical island where you can finally start your vacation. As you reach the waiting area to board the ferry, you realize you're so early, nobody else has even arrived yet!

By modeling the process people use to choose (or abandon) their seat in the waiting area, you're pretty sure you can predict the best place to sit. You make a quick map of the seat layout (your puzzle input).

The seat layout fits neatly on a grid. Each position is either floor (.), an empty seat (L), or an occupied seat (#). For example, the initial seat layout might look like this:

L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

Now, you just need to model the people who will be arriving shortly. Fortunately, people are entirely predictable and always follow a simple set of rules. All decisions are based on the number of occupied seats adjacent to a given seat (one of the eight positions immediately up, down, left, right, or diagonal from the seat). The following rules are applied to every seat simultaneously:

    If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
    If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
    Otherwise, the seat's state does not change.

Floor (.) never changes; seats don't move, and nobody sits on the floor.

After one round of these rules, every seat in the example layout becomes occupied:

#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##

After a second round, the seats with four or more occupied adjacent seats become empty again:

#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##

This process continues for three more rounds:

#.##.L#.##
#L###LL.L#
L.#.#..#..
#L##.##.L#
#.##.LL.LL
#.###L#.##
..#.#.....
#L######L#
#.LL###L.L
#.#L###.##

#.#L.L#.##
#LLL#LL.L#
L.L.L..#..
#LLL.##.L#
#.LL.LL.LL
#.LL#L#.##
..L.L.....
#L#LLLL#L#
#.LLLLLL.L
#.#L#L#.##

#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##

At this point, something interesting happens: the chaos stabilizes and further applications of these rules cause no seats to change state! Once people stop moving around, you count 37 occupied seats.

Simulate your seating area by applying the seating rules repeatedly until no seats change state. How many seats end up occupied?

--- Part Two ---

As soon as people start to arrive, you realize your mistake. People don't just care about adjacent seats - they care about the first seat they can see in each of those eight directions!

Now, instead of considering just the eight immediately adjacent seats, consider the first seat in each of those eight directions. For example, the empty seat below would see eight occupied seats:

.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#.....

The leftmost empty seat below would only see one empty seat, but cannot see any of the occupied ones:

.............
.L.L.#.#.#.#.
.............

The empty seat below would see no occupied seats:

.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.

Also, people seem to be more tolerant than you expected: it now takes five or more visible occupied seats for an occupied seat to become empty (rather than four or more from the previous rules). The other rules still apply: empty seats that see no occupied seats become occupied, seats matching no rule don't change, and floor never changes.

Given the same starting layout as above, these new rules cause the seating area to shift around as follows:

L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##

#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#

#.L#.##.L#
#L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
#.#####.#L
..#.#.....
LLL####LL#
#.L#####.L
#.L####.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
#.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
#.L####.LL
..#.#.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
#.LLLL#.LL
..#.L.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#

Again, at this point, people stop shifting around and the seating area reaches equilibrium. Once this occurs, you count 26 occupied seats.

Given the new visibility method and the rule change for occupied seats becoming empty, once equilibrium is reached, how many seats end up occupied? */

:- use_module(library(apply), [maplist/4, maplist/3, foldl/4]).
:- use_module(library(aggregate), [aggregate_all/3]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, integer//1]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [last/2, member/2, nth0/3]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).

seats(X, Y, [seat(X, Y)|Seats]) -->
    "L",
    { X0 is X + 1 },
    seats(X0, Y, Seats).
seats(_, Y, Seats) -->
    "\n",
    { Y0 is Y + 1 },
    seats(0, Y0, Seats).
seats(X0, Y, Seats) -->
    ".",
    { X1 is X0 + 1 },
    seats(X1, Y, Seats).
seats(_, _, []) --> [].

:- dynamic seat_state/3.

solve1(Input, Answer) :- % 2489
    phrase_from_file(seats(0, 0, Seats), Input), !,
    retractall(seat_state(_, _, _)),
    forall(
        member(seat(X, Y), Seats),
        assertz(seat_state(X, Y, empty))
    ),
    advance_until_fixed(0),
    aggregate_all(count, seat_state(_, _, occupied), Answer).

advance_until_fixed(N0) :-
    ( 0 is N0 mod 10 -> debug(xxx, "Gen ~w", [N0]) ; true ),
    next_state(Next),
    ( state_fixed_point(Next)
    -> true
    ;  ( N1 is N0 + 1,
         advance_state(Next),
         advance_until_fixed(N1)
       )
    ).

state_fixed_point(Next) :-
    forall(
        member(State, Next),
        State
    ).

advance_state(Next) :-
    retractall(seat_state(_, _, _)),
    forall(member(S, Next), assertz(S)).

next_state(Next) :-
    findall(
        seat_state(X, Y, SeatState),
        ( seat_state(X, Y, S),
          neighbours(X, Y, NEmpty, NOccupied),
          next_seat_state(S, NEmpty, NOccupied, SeatState)
        ),
        Next
    ).

next_seat_state(empty, _, 0, occupied) :- !.
next_seat_state(occupied, _, NOccupied, empty) :- NOccupied >= 4, !.
next_seat_state(S, _, _, S).

neighbours(X, Y, NEmpty, NOccupied) :-
    findall(
        Sn,
        ( X0 is X - 1, X1 is X + 1,
          Y0 is Y - 1, Y1 is Y + 1,
          between(X0, X1, Xn),
          between(Y0, Y1, Yn),
          X-Y \== Xn-Yn,
          seat_state(Xn, Yn, Sn)
        ),
        States
    ),
    foldl(count_empty_occupied, States, 0-0, NEmpty-NOccupied).

count_empty_occupied(empty, E0-O, E1-O) :- E1 is E0 + 1.
count_empty_occupied(occupied, E-O0, E-O1) :- O1 is O0 + 1.

solve2(Input, Answer) :-
    phrase_from_file(seats(0, 0, Seats), Input), !,
    retractall(seat_state(_, _, _)),
    forall(
        member(seat(X, Y), Seats),
        assertz(seat_state(X, Y, empty))
    ),
    aggregate_all(max(X), seat_state(X, _, _), MaxX),
    aggregate_all(max(Y), seat_state(_, Y, _), MaxY),
    advance_until_fixed2(MaxX, MaxY, 0),
    aggregate_all(count, seat_state(_, _, occupied), Answer).

direction(Dx, Dy) :-
    member(Dx, [-1, 0, 1]),
    member(Dy, [-1, 0, 1]),
    Dx-Dy \== 0-0.

neighbours2(X, Y, NEmpty, NOccupied, MaxX-MaxY) :-
    findall(
        Sn,
        ( direction(Dx, Dy),
          nearest_neighbour(X, Y, Dx, Dy, MaxX, MaxY, Sn)
        ),
        States
    ),
    foldl(count_empty_occupied, States, 0-0, NEmpty-NOccupied).

nearest_neighbour(X, Y, Dx, Dy, MaxX, MaxY, Sn) :-
    Xn is X + Dx, Yn is Y + Dy,
    between(0, MaxX, Xn), between(0, MaxY, Yn),
    (  seat_state(Xn, Yn, Sn)
    -> true
    ;  nearest_neighbour(Xn, Yn, Dx, Dy, MaxX, MaxY, Sn)
    ).

advance_until_fixed2(X, Y, N0) :-
    ( 0 is N0 mod 10 -> debug(xxx, "Gen ~w", [N0]) ; true ),
    time(next_state2(X, Y, Next)),
    ( state_fixed_point(Next)
    -> true
    ;  ( N1 is N0 + 1,
         advance_state(Next),
         advance_until_fixed2(X, Y, N1)
       )
    ).

next_seat_state2(empty, _, 0, occupied) :- !.
next_seat_state2(occupied, _, NOccupied, empty) :- NOccupied >= 5, !.
next_seat_state2(S, _, _, S).

next_state2(MaxX, MaxY, Next) :-
    findall(
        seat_state(X, Y, SeatState),
        ( seat_state(X, Y, S),
          neighbours2(X, Y, NEmpty, NOccupied, MaxX-MaxY),
          next_seat_state2(S, NEmpty, NOccupied, SeatState)
        ),
        Next
    ).
