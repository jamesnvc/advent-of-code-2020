:- module(p03, [solve1/2, solve2/2]).
/*
*--- Day 3: Toboggan Trajectory ---

With the toboggan login problems resolved, you set off toward the airport. While travel by toboggan might be easy, it's certainly not safe: there's very minimal steering and the area is covered in trees. You'll need to see which angles will take you near the fewest trees.

Due to the local geology, trees in this area only grow on exact integer coordinates in a grid. You make a map (your puzzle input) of the open squares (.) and trees (#) you can see. For example:

..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#

These aren't the only trees, though; due to something you read about once involving arboreal genetics and biome stability, the same pattern repeats to the right many times:

..##.........##.........##.........##.........##.........##.......  --->
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#  --->

You start on the open square (.) in the top-left corner and need to reach the bottom (below the bottom-most row on your map).

The toboggan can only follow a few specific slopes (you opted for a cheaper model that prefers rational numbers); start by counting all the trees you would encounter for the slope right 3, down 1:

From your starting position at the top-left, check the position that is right 3 and down 1. Then, check the position that is right 3 and down 1 from there, and so on until you go past the bottom of the map.

The locations you'd check in the above example are marked here with O where there was an open square and X where there was a tree:

..##.........##.........##.........##.........##.........##.......  --->
#..O#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....X..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#O#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..X...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.X#.......#.##.......#.##.......#.##.......#.##.....  --->
.#.#.#....#.#.#.#.O..#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........X.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.X#...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...#X....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...X.#.#..#...#.#.#..#...#.#.#..#...#.#  --->

In this example, traversing the map using this slope would cause you to encounter 7 trees.

Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many trees would you encounter?

--- Part Two ---

Time to check the rest of the slopes - you need to minimize the probability of a sudden arboreal stop, after all.

Determine the number of trees you would encounter if, for each of the following slopes, you start at the top-left corner and traverse the map all the way to the bottom:

    Right 1, down 1.
    Right 3, down 1. (This is the slope you already checked.)
    Right 5, down 1.
    Right 7, down 1.
    Right 1, down 2.

In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied together, these produce the answer 336.

What do you get if you multiply together the number of trees encountered on each of the listed slopes? */

:- use_module(library(clpfd)).
:- use_module(library(readutil), [read_line_to_codes/2]).
:- use_module(library(lists), [nth0/3]).
:- use_module(library(debug), [debug/3]).

read_lines(S, Ls) :-
    read_line_to_codes(S, Line),
    ( Line = end_of_file
    -> Ls = []
    ;  ( Ls = [Line|Ls1],
         read_lines(S, Ls1)
       )
    ).

read_grid(File, Grid) :-
    setup_call_cleanup(
        open(File, read, S),
        read_lines(S, Grid),
        close(S)
    ).

grid_location(G, X, Y, C) :-
    nth0(Y, G, Line),
    length(Line, LineL),
    Xx is X mod LineL,
    nth0(Xx, Line, C).

walk_grid(G, X-Y, Trees0, Trees) :-
    walk_grid_step(G, 3-1, X-Y, Trees0, Trees).

walk_grid_step(G, _-_, _-Y, Trees0, Trees0) :-
    length(G, Gl),
    Gl #= Y + 1,
    !.
walk_grid_step(G, Dx-Dy, X0-Y0, Trees0, Trees) :-
    X1 #= X0 + Dx,
    Y1 #= Y0 + Dy,
    grid_location(G, X1, Y1, C),
    ( C == 0'#
    -> ( succ(Trees0, Trees1),
         walk_grid_step(G, Dx-Dy, X1-Y1, Trees1, Trees) )
    ; walk_grid_step(G, Dx-Dy, X1-Y1, Trees0, Trees)
    ).

solve1(Input, Answer) :-
    read_grid(Input, Grid),
    walk_grid(Grid, 0-0, 0, Answer).

solve2(Input, Answer) :-
    read_grid(Input, Grid),
    walk_grid_step(Grid, 1-1, 0-0, 0, A1),
    walk_grid_step(Grid, 3-1, 0-0, 0, A2),
    walk_grid_step(Grid, 5-1, 0-0, 0, A3),
    walk_grid_step(Grid, 7-1, 0-0, 0, A4),
    walk_grid_step(Grid, 1-2, 0-0, 0, A5),
    Answer #= A1 * A2 * A3 * A4 * A5.
