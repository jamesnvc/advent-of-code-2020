:- module(p17, []).
/*
--- Day 17: Conway Cubes ---

As your flight slowly drifts through the sky, the Elves at the Mythical Information Bureau at the North Pole contact you. They'd like some help debugging a malfunctioning experimental energy source aboard one of their super-secret imaging satellites.

The experimental energy source is based on cutting-edge technology: a set of Conway Cubes contained in a pocket dimension! When you hear it's having problems, you can't help but agree to take a look.

The pocket dimension contains an infinite 3-dimensional grid. At every integer 3-dimensional coordinate (x,y,z), there exists a single cube which is either active or inactive.

In the initial state of the pocket dimension, almost all cubes start inactive. The only exception to this is a small flat region of cubes (your puzzle input); the cubes in this region start in the specified active (#) or inactive (.) state.

The energy source then proceeds to boot up by executing six cycles.

Each cube only ever considers its neighbors: any of the 26 other cubes where any of their coordinates differ by at most 1. For example, given the cube at x=1,y=2,z=3, its neighbors include the cube at x=2,y=2,z=2, the cube at x=0,y=2,z=3, and so on.

During a cycle, all cubes simultaneously change their state according to the following rules:

    If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active. Otherwise, the cube becomes inactive.
    If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active. Otherwise, the cube remains inactive.

The engineers responsible for this experimental energy source would like you to simulate the pocket dimension and determine what the configuration of cubes should be at the end of the six-cycle boot process.

For example, consider the following initial state:

.#.
..#
###

Even though the pocket dimension is 3-dimensional, this initial state represents a small 2-dimensional slice of it. (In particular, this initial state defines a 3x3x1 region of the 3-dimensional space.)

Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is shown layer-by-layer at each given z coordinate (and the frame of view follows the active cells in each cycle):

Before any cycles:

z=0
.#.
..#
###


After 1 cycle:

z=-1
#..
..#
.#.

z=0
#.#
.##
.#.

z=1
#..
..#
.#.


After 2 cycles:

z=-2
.....
.....
..#..
.....
.....

z=-1
..#..
.#..#
....#
.#...
.....

z=0
##...
##...
#....
....#
.###.

z=1
..#..
.#..#
....#
.#...
.....

z=2
.....
.....
..#..
.....
.....


After 3 cycles:

z=-2
.......
.......
..##...
..###..
.......
.......
.......

z=-1
..#....
...#...
#......
.....##
.#...#.
..#.#..
...#...

z=0
...#...
.......
#......
.......
.....##
.##.#..
...#...

z=1
..#....
...#...
#......
.....##
.#...#.
..#.#..
...#...

z=2
.......
.......
..##...
..###..
.......
.......
.......

After the full six-cycle boot process completes, 112 cubes are left in the active state.

Starting with your given initial configuration, simulate six cycles. How many cubes are left in the active state after the sixth cycle?

--- Part Two ---

For some reason, your simulated results don't match what the experimental energy source engineers expected. Apparently, the pocket dimension actually has four spatial dimensions, not three.

The pocket dimension contains an infinite 4-dimensional grid. At every integer 4-dimensional coordinate (x,y,z,w), there exists a single cube (really, a hypercube) which is still either active or inactive.

Each cube only ever considers its neighbors: any of the 80 other cubes where any of their coordinates differ by at most 1. For example, given the cube at x=1,y=2,z=3,w=4, its neighbors include the cube at x=2,y=2,z=3,w=3, the cube at x=0,y=2,z=3,w=4, and so on.

The initial state of the pocket dimension still consists of a small flat region of cubes. Furthermore, the same rules for cycle updating still apply: during each cycle, consider the number of active neighbors of each cube.

For example, consider the same initial state as in the example above. Even though the pocket dimension is 4-dimensional, this initial state represents a small 2-dimensional slice of it. (In particular, this initial state defines a 3x3x1x1 region of the 4-dimensional space.)

Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is shown layer-by-layer at each given z and w coordinate:

Before any cycles:

z=0, w=0
.#.
..#
###


After 1 cycle:

z=-1, w=-1
#..
..#
.#.

z=0, w=-1
#..
..#
.#.

z=1, w=-1
#..
..#
.#.

z=-1, w=0
#..
..#
.#.

z=0, w=0
#.#
.##
.#.

z=1, w=0
#..
..#
.#.

z=-1, w=1
#..
..#
.#.

z=0, w=1
#..
..#
.#.

z=1, w=1
#..
..#
.#.


After 2 cycles:

z=-2, w=-2
.....
.....
..#..
.....
.....

z=-1, w=-2
.....
.....
.....
.....
.....

z=0, w=-2
###..
##.##
#...#
.#..#
.###.

z=1, w=-2
.....
.....
.....
.....
.....

z=2, w=-2
.....
.....
..#..
.....
.....

z=-2, w=-1
.....
.....
.....
.....
.....

z=-1, w=-1
.....
.....
.....
.....
.....

z=0, w=-1
.....
.....
.....
.....
.....

z=1, w=-1
.....
.....
.....
.....
.....

z=2, w=-1
.....
.....
.....
.....
.....

z=-2, w=0
###..
##.##
#...#
.#..#
.###.

z=-1, w=0
.....
.....
.....
.....
.....

z=0, w=0
.....
.....
.....
.....
.....

z=1, w=0
.....
.....
.....
.....
.....

z=2, w=0
###..
##.##
#...#
.#..#
.###.

z=-2, w=1
.....
.....
.....
.....
.....

z=-1, w=1
.....
.....
.....
.....
.....

z=0, w=1
.....
.....
.....
.....
.....

z=1, w=1
.....
.....
.....
.....
.....

z=2, w=1
.....
.....
.....
.....
.....

z=-2, w=2
.....
.....
..#..
.....
.....

z=-1, w=2
.....
.....
.....
.....
.....

z=0, w=2
###..
##.##
#...#
.#..#
.###.

z=1, w=2
.....
.....
.....
.....
.....

z=2, w=2
.....
.....
..#..
.....
.....

After the full six-cycle boot process completes, 848 cubes are left in the active state.

Starting with your given initial configuration, simulate six cycles in a 4-dimensional space. How many cubes are left in the active state after the sixth cycle? */

:- use_module(library(apply), [maplist/4, maplist/3, foldl/4, exclude/3, include/3]).
:- use_module(library(aggregate), [aggregate_all/3, foreach/2]).
:- use_module(library(dcg/basics), [string_without//2, blank//0, integer//1]).
:- use_module(library(ordsets), [ord_del_element/3, ord_union/3]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [select/3, member/2, permutation/2, nth1/3, sum_list/2]).
:- use_module(library(pairs), [group_pairs_by_key/2]).
:- use_module(library(debug), [debug/3]).
:- use_module(library(readutil), [read_file_to_string/3]).
:- use_module(library(statistics), [time/1]).
:- use_module(library(thread), [concurrent_forall/2]).
:- use_module(library(clpfd)).
:- use_module(library(yall)).

:- dynamic generation_coords_active/4.

setup_grid(Input) :-
    read_file_to_codes(Input, Contents, []),
    assert_grid(0, 0, Contents).

assert_grid(_, _, []).
assert_grid(_, Y0, [0'\n|Cs]) :-
    Y1 is Y0 + 1,
    assert_grid(0, Y1, Cs).
assert_grid(X0, Y0, [0'.|Cs]) :-
    X1 is X0 + 1,
    assert_grid(X1, Y0, Cs).
assert_grid(X0, Y0, [0'#|Cs]) :-
    assertz(generation_coords_active(0, X0, Y0, 0)),
    X1 is X0 + 1,
    assert_grid(X1, Y0, Cs).

solve1(Input, Answer) :-
    retractall(generation_coords_active(_, _, _, _)),
    setup_grid(Input),
    run_generations(0, 6),
    aggregate_all(count, generation_coords_active(6, _, _, _), Answer).

run_generations(N, N) :- !.
run_generations(N0, End) :-
    step_gen(N0),
    N1 is N0 + 1,
    run_generations(N1, End).

step_gen(G) :-
    G1 is G + 1,
    % active -> inactive
    concurrent_forall(
        generation_coords_active(G, X, Y, Z),
        ( active_neighbours(G, X, Y, Z, Count),
          ( between(2, 3, Count)
          -> assertz(generation_coords_active(G1, X, Y, Z))
          ;  true )
        )
    ),
    % inactive -> active
    gen_bounds(G, Mins, Maxes),
    concurrent_forall(
        empty_coords_between(G, Mins, Maxes, X-Y-Z),
        ( active_neighbours(G, X, Y, Z, 3)
        -> assertz(generation_coords_active(G1, X, Y, Z))
        ;  true )
    ).

empty_coords_between(G, Xl0-Yl0-Zl0, Xh0-Yh0-Zh0, Xn-Yn-Zn) :-
    Xl is Xl0 - 1, Yl is Yl0 - 1, Zl is Zl0 - 1,
    Xh is Xh0 + 1, Yh is Yh0 + 1, Zh is Zh0 + 1,
    between(Xl, Xh, Xn),
    between(Yl, Yh, Yn),
    between(Zl, Zh, Zn),
    \+ generation_coords_active(G, Xn, Yn, Zn).

gen_bounds(G, Xl-Yl-Zl, Xh-Yh-Zh) :-
    aggregate_all(
        x(min(X), min(Y), min(Z),
          max(X), max(Y), max(Z)),
        generation_coords_active(G, X, Y, Z),
        x(Xl, Yl, Zl, Xh, Yh, Zh)
    ).

active_neighbours(G, X, Y, Z, Count) :-
    Xl is X - 1, Xh is X + 1,
    Yl is Y - 1, Yh is Y + 1,
    Zl is Z - 1, Zh is Z + 1,
    aggregate_all(count,
                  ( between(Xl, Xh, Xn),
                    between(Yl, Yh, Yn),
                    between(Zl, Zh, Zn),
                    Xn-Yn-Zn \== X-Y-Z,
                    generation_coords_active(G, Xn, Yn, Zn)
                  ),
                  Count).

:- dynamic generation_coords_active/5.

setup_grid2(Input) :-
    read_file_to_codes(Input, Contents, []),
    assert_grid2(0, 0, Contents).

assert_grid2(_, _, []).
assert_grid2(_, Y0, [0'\n|Cs]) :-
    Y1 is Y0 + 1,
    assert_grid2(0, Y1, Cs).
assert_grid2(X0, Y0, [0'.|Cs]) :-
    X1 is X0 + 1,
    assert_grid2(X1, Y0, Cs).
assert_grid2(X0, Y0, [0'#|Cs]) :-
    assertz(generation_coords_active(0, X0, Y0, 0, 0)),
    X1 is X0 + 1,
    assert_grid2(X1, Y0, Cs).

solve2(Input, Answer) :-
    retractall(generation_coords_active(_, _, _, _, _)),
    setup_grid2(Input),
    run_generations2(0, 6),
    aggregate_all(count, generation_coords_active(6, _, _, _, _), Answer).

run_generations2(N, N) :- !.
run_generations2(N0, End) :-
    step_gen2(N0),
    N1 is N0 + 1,
    run_generations2(N1, End).

step_gen2(G) :-
    G1 is G + 1,
    % active -> inactive
    concurrent_forall(
        generation_coords_active(G, X, Y, Z, W),
        ( active_neighbours(G, X, Y, Z, W, Count),
          ( between(2, 3, Count)
          -> assertz(generation_coords_active(G1, X, Y, Z, W))
          ;  true )
        )
    ),
    % inactive -> active
    gen_bounds2(G, Mins, Maxes),
    concurrent_forall(
        empty_coords_between2(G, Mins, Maxes, X-Y-Z-W),
        ( active_neighbours(G, X, Y, Z, W, 3)
        -> assertz(generation_coords_active(G1, X, Y, Z, W))
        ;  true )
    ).

empty_coords_between2(G, Xl0-Yl0-Zl0-Wl0, Xh0-Yh0-Zh0-Wh0, Xn-Yn-Zn-Wn) :-
    Xl is Xl0 - 1, Yl is Yl0 - 1, Zl is Zl0 - 1, Wl is Wl0 - 1,
    Xh is Xh0 + 1, Yh is Yh0 + 1, Zh is Zh0 + 1, Wh is Wh0 + 1,
    between(Xl, Xh, Xn),
    between(Yl, Yh, Yn),
    between(Zl, Zh, Zn),
    between(Wl, Wh, Wn),
    \+ generation_coords_active(G, Xn, Yn, Zn, Wn).

gen_bounds2(G, Xl-Yl-Zl-Wl, Xh-Yh-Zh-Wh) :-
    aggregate_all(
        x(min(X), min(Y), min(Z), min(W),
          max(X), max(Y), max(Z), max(W)),
        generation_coords_active(G, X, Y, Z, W),
        x(Xl, Yl, Zl, Wl, Xh, Yh, Zh, Wh)
    ).

active_neighbours(G, X, Y, Z, W, Count) :-
    Xl is X - 1, Xh is X + 1,
    Yl is Y - 1, Yh is Y + 1,
    Zl is Z - 1, Zh is Z + 1,
    Wl is W - 1, Wh is W + 1,
    aggregate_all(count,
                  ( between(Xl, Xh, Xn),
                    between(Yl, Yh, Yn),
                    between(Zl, Zh, Zn),
                    between(Wl, Wh, Wn),
                    Xn-Yn-Zn-Wn \== X-Y-Z-W,
                    generation_coords_active(G, Xn, Yn, Zn, Wn)
                  ),
                  Count).
