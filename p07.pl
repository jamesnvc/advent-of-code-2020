:- module(p07, []).
/*
--- Day 7: Handy Haversacks ---

You land at the regional airport in time for your next flight. In fact, it looks like you'll even have time to grab some food: all flights are currently delayed due to issues in luggage processing.

Due to recent aviation regulations, many rules (your puzzle input) are being enforced about bags and their contents; bags must be color-coded and must contain specific quantities of other color-coded bags. Apparently, nobody responsible for these regulations considered how long they would take to enforce!

For example, consider the following rules:

light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.

These rules specify the required contents for 9 bag types. In this example, every faded blue bag is empty, every vibrant plum bag contains 11 bags (5 faded blue and 6 dotted black), and so on.

You have a shiny gold bag. If you wanted to carry it in at least one other bag, how many different bag colors would be valid for the outermost bag? (In other words: how many colors can, eventually, contain at least one shiny gold bag?)

In the above rules, the following options would be available to you:

    A bright white bag, which can hold your shiny gold bag directly.
    A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.
    A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
    A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.

So, in this example, the number of bag colors that can eventually contain at least one shiny gold bag is 4.

How many bag colors can eventually contain at least one shiny gold bag? (The list of rules is quite long; make sure you get all of it.)

--- Part Two ---

It's getting pretty expensive to fly these days - not because of ticket prices, but because of the ridiculous number of bags you need to buy!

Consider again your shiny gold bag and the rules from the above example:

    faded blue bags contain 0 other bags.
    dotted black bags contain 0 other bags.
    vibrant plum bags contain 11 other bags: 5 faded blue bags and 6 dotted black bags.
    dark olive bags contain 7 other bags: 3 faded blue bags and 4 dotted black bags.

So, a single shiny gold bag must contain 1 dark olive bag (and the 7 bags within it) plus 2 vibrant plum bags (and the 11 bags within each of those): 1 + 1*7 + 2 + 2*11 = 32 bags!

Of course, the actual rules have a small chance of going several levels deeper than this example; be sure to count all of the bags, even if the nesting becomes topologically impractical!

Here's another example:

shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.

In this example, a single shiny gold bag must contain 126 other bags.

How many individual bags are required inside your single shiny gold bag? */

:- use_module(library(aggregate), [aggregate_all/3]).
:- use_module(library(dcg/basics), [string_without//2, string//1, integer//1]).
:- use_module(library(pure_input), [phrase_from_file/2]).
:- use_module(library(lists), [member/2]).

rule(rule(Colour, Contains)) -->
    string(ColourC), " bags contain ",
    { string_codes(Colour, ColourC) },
    contain_rules(Contains).

optional(C) --> C.
optional(_) --> [].

contain_rules([]) --> "no other bags.".
contain_rules([contain(Num, Colour)|Contains]) -->
    integer(Num), " ", string(ColourC), " bag", optional("s"),
    { string_codes(Colour, ColourC) },
    contains_rules_next(Contains).
contains_rules_next([]) --> ".".
contains_rules_next(Contains) --> ", ", contain_rules(Contains).

rules([Rule|Rules]) -->
    rule(Rule), !, "\n", rules(Rules).
rules([]) --> [].

:- dynamic([bag_contains/3], [incremental(true)]).

:- table bag_containable/2 as incremental.

bag_containable(Colour, Container) :-
    bag_contains(Container, _, Colour).
bag_containable(Colour, Container) :-
    bag_containable(Colour, Con1),
    bag_contains(Container, _, Con1).

load_rules(Input) :-
    phrase_from_file(rules(Rules), Input),
    retractall(bag_contains(_, _, _)),
    forall(
        ( member(rule(Colour, Cs), Rules),
          member(contain(N, Colour2), Cs)
        ),
        assertz(bag_contains(Colour, N, Colour2))
    ).

solve1(Input, Answer) :-
    load_rules(Input),
    setof(Cons,
          bag_containable("shiny gold", Cons),
          Possibilities),
    length(Possibilities, Answer).

contained_in(Bag, Contents) :-
    aggregate_all(sum(N),
                  ( bag_contains(Bag, N0, Subbag),
                    contained_in(Subbag, N1),
                    N is N0 + N0 * N1
                  ),
                 Contents).

solve2(Input, Answer) :-
    load_rules(Input),
    contained_in("shiny gold", Answer).
